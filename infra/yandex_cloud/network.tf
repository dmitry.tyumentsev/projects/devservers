resource "yandex_vpc_network" "vpc_network" {
  name = "${var.resource_name_prefix}network"
}

resource "yandex_vpc_subnet" "vpc_subnet" {
  name           = "${var.resource_name_prefix}subnet"
  zone           = var.yc_region_zone
  network_id     = yandex_vpc_network.vpc_network.id
  v4_cidr_blocks = var.vpc_subnet_v4_cidr_blocks
}

variable "resource_name_prefix" {
  description = "Prefix to define resource names"
  type        = string
  default     = "devservers-"
}

variable "yc_iam_token" {
  description = "Yandex token `yc iam create-token`"
}

variable "yc_cloud_id" {
  description = "Yandex cloud id `yc config get cloud-id`"
}

variable "yc_folder_id" {
  description = "Yandex folder id `yc config get folder-id`"
}

variable "yc_region_zone" {
  description = "Yandex region `yc compute zone list`"
  type        = string
  default     = "ru-central1-b"
}

variable "vpc_subnet_v4_cidr_blocks" {
  description = "VPC subnet IPv4 CIDR blocks"
  type        = list(string)
  default     = ["192.168.10.0/24"]
}

# Gitea Instance
variable "gitea_instance_hostname" {
  description = "Gitea instance resource hostname"
  default     = "gitea"
  type        = string
}

variable "gitea_instance_labels" {
  description = "A set of key/value label pairs to assign to the Gitea instance."
  default     = {}
  type        = map(string)
}

variable "gitea_image_family" {
  description = "Distro image family for gitea instance"
  default     = "debian-10"
  type        = string
}

variable "gitea_instance_platform_id" {
  description = "Available processors: https://cloud.yandex.com/en-ru/docs/compute/concepts/vm-platforms"
  type        = string
  default     = "standard-v3"
}

variable "gitea_instance_cores" {
  description = "Amount of CPU cores for gitea instance"
  default     = 2
  type        = number
}

variable "gitea_instance_memory" {
  description = "Amount of RAM, GiB for gitea instance"
  default     = 2
  type        = number
}

variable "gitea_instance_core_fraction" {
  description = "Guaranteed share of vCPU % for gitea instance"
  default     = 100
  type        = number
}

variable "is_gitea_instance_preemptible" {
  description = "Specifies if the instance is preemptible"
  default     = false
  type        = bool
}

variable "is_gitea_instance_serial_port_enabled" {
  description = "Flag allowing access to the serial console"
  default     = true
  type        = bool
}

variable "is_gitea_instance_public" {
  description = "Is the gitea instance supposed to have an external ip"
  default     = true
  type        = bool
}

variable "gitea_instance_disk_type" {
  description = "Gitea instance disk type"
  type        = string
  default     = "network-hdd"
}

variable "gitea_instance_disk_size" {
  description = "Gitea instance disk size, GiB"
  type        = number
  default     = 11
}

variable "gitea_instance_ssh_user" {
  description = "Gitea instance username for ssh access"
  type        = string
  default     = "ansible"
}

variable "gitea_instance_ssh_public_key_path" {
  description = "Gitea instance ssh public key path for ssh access"
  type        = string
  default     = "~/.ssh/id_ed25519.pub"
}

# Drone Instance
variable "drone_instance_hostname" {
  description = "drone instance resource hostname"
  default     = "drone"
  type        = string
}

variable "drone_instance_labels" {
  description = "A set of key/value label pairs to assign to the drone instance."
  default     = {}
  type        = map(string)
}

variable "drone_image_family" {
  description = "Distro image family for drone instance"
  default     = "debian-10"
  type        = string
}

variable "drone_instance_platform_id" {
  description = "Available processors: https://cloud.yandex.com/en-ru/docs/compute/concepts/vm-platforms"
  type        = string
  default     = "standard-v3"
}

variable "drone_instance_cores" {
  description = "Amount of CPU cores for drone instance"
  default     = 2
  type        = number
}

variable "drone_instance_memory" {
  description = "Amount of RAM, GiB for drone instance"
  default     = 2
  type        = number
}

variable "drone_instance_core_fraction" {
  description = "Guaranteed share of vCPU % for drone instance"
  default     = 100
  type        = number
}

variable "is_drone_instance_preemptible" {
  description = "Specifies if the instance is preemptible"
  default     = false
  type        = bool
}

variable "is_drone_instance_public" {
  description = "Is the drone instance supposed to have an external ip"
  default     = true
  type        = bool
}


variable "is_drone_instance_serial_port_enabled" {
  description = "Flag allowing access to the serial console"
  default     = true
  type        = bool
}

variable "drone_instance_disk_type" {
  description = "Drone instance disk type"
  type        = string
  default     = "network-hdd"
}

variable "drone_instance_disk_size" {
  description = "Drone instance disk size, GiB"
  type        = number
  default     = 11
}

variable "drone_instance_ssh_user" {
  description = "Drone instance username for ssh access"
  type        = string
  default     = "ansible"
}

variable "drone_instance_ssh_public_key_path" {
  description = "Drone instance ssh public key path for ssh access"
  type        = string
  default     = "~/.ssh/id_ed25519.pub"
}

output "internal_gitea_ip" {
  value = module.gitea_instance.internal_instance_ip
}

output "external_gitea_ip" {
  value = module.gitea_instance.external_instance_ip
}

output "internal_drone_ip" {
  value = module.drone_instance.internal_instance_ip
}

output "external_drone_ip" {
  value = module.drone_instance.external_instance_ip
}

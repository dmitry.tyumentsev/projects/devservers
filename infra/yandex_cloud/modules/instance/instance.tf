data "yandex_compute_image" "instance_compute_image" {
  family = var.instance_image_family
}

data "template_file" "instance_user_data" {
  template = file("${path.module}/templates/user-data.tpl")
  vars = {
    ssh_user       = var.instance_ssh_user
    ssh_public_key = file(var.instance_ssh_public_key_path)
  }
}

resource "yandex_compute_instance" "this" {
  name        = var.instance_name
  labels      = var.instance_labels
  platform_id = var.instance_platform_id

  resources {
    cores         = var.instance_cores
    memory        = var.instance_memory
    core_fraction = var.instance_core_fraction
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.instance_compute_image.id
      size     = var.instance_disk_size
      type     = var.instance_disk_type
    }
  }


  lifecycle {
    ignore_changes = [boot_disk[0].initialize_params[0].image_id]
  }

  network_interface {
    subnet_id = var.instance_vpc_subnet_id
    nat       = var.is_instance_public
  }

  scheduling_policy {
    preemptible = var.is_instance_preemptible
  }

  metadata = {
    user-data          = data.template_file.instance_user_data.rendered
    serial-port-enable = var.is_instance_serial_port_enabled ? "1" : "0"
  }
}

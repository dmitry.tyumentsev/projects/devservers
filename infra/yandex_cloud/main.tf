module "gitea_instance" {
  source = "./modules/instance"

  instance_name                = "${var.resource_name_prefix}gitea"
  instance_vpc_subnet_id       = yandex_vpc_subnet.vpc_subnet.id
  instance_hostname            = var.gitea_instance_hostname
  instance_labels              = var.gitea_instance_labels
  instance_image_family        = var.gitea_image_family
  instance_platform_id         = var.gitea_instance_platform_id
  instance_cores               = var.gitea_instance_cores
  instance_memory              = var.gitea_instance_memory
  instance_core_fraction       = var.gitea_instance_core_fraction
  is_instance_preemptible      = var.is_gitea_instance_preemptible
  is_instance_public           = var.is_gitea_instance_public
  instance_disk_size           = var.gitea_instance_disk_size
  instance_disk_type           = var.gitea_instance_disk_type
  instance_ssh_user            = var.gitea_instance_ssh_user
  instance_ssh_public_key_path = var.gitea_instance_ssh_public_key_path
}

module "drone_instance" {
  source = "./modules/instance"

  instance_name                = "${var.resource_name_prefix}drone"
  instance_vpc_subnet_id       = yandex_vpc_subnet.vpc_subnet.id
  instance_hostname            = var.drone_instance_hostname
  instance_labels              = var.drone_instance_labels
  instance_image_family        = var.drone_image_family
  instance_platform_id         = var.drone_instance_platform_id
  instance_cores               = var.drone_instance_cores
  instance_memory              = var.drone_instance_memory
  instance_core_fraction       = var.drone_instance_core_fraction
  is_instance_preemptible      = var.is_drone_instance_preemptible
  is_instance_public           = var.is_drone_instance_public
  instance_disk_size           = var.drone_instance_disk_size
  instance_disk_type           = var.drone_instance_disk_type
  instance_ssh_user            = var.drone_instance_ssh_user
  instance_ssh_public_key_path = var.drone_instance_ssh_public_key_path
}

# Автоматическая конфигурация сервера
Плейбук устанавливает систему контроля версий Gitea и средство непрерывной интеграции Drone.

## Требование к серверу
Нужен пользователь с правами sudo.

## Склонировать проект
`git clone https://gitlab.com/dmitry.tyumentsev/projects/devservers.git`

`cd devservers`

## Установить ansible, yandex cloud sdk
`pip install -r requirements.txt`

## Установить ansible роли
`ansible-galaxy install -r requirements.yml`

## Тестовый запуск с помощью Vagrant и VirtalBox
Действия происходят на компьютере администратора.

### Поднять виртуальные машины
`cd infra/vagrant`

`vagrant up`

### Запустить плейбук
`cd -`

`ansible-playbook playbook.yml -i inventories/vagrant/hosts.yml`

### Доступ к приложениям
| IP адрес          | Сервис |
| ----------------- | ------ |
| 192.168.56.4:3000 | Gitea  |
| 192.168.56.5:4000 | Drone  |

### Удалить виртуальные машины
`cd infra/vagrant`

`vagrant destroy -f`

## Запуск с помощью Terraform на Yandex Cloud
Действия происходят на компьютере администратора.

### Указать секретные переменные
```shell
export TF_VAR_yc_iam_token=$(yc iam create-token)
export TF_VAR_yc_cloud_id=$(yc config get cloud-id)
export TF_VAR_yc_folder_id=$(yc config get folder-id)
```

### Поднять виртуальные машины
`cd infra/yandex_cloud`

`terraform apply -var-file=vars.tfvars`

### Запустить плейбук
`cd -`

`chmod +x inventories/yandex_cloud/hosts.py`

`ansible-playbook playbook.yml -i inventories/yandex_cloud/hosts.py`

### Удалить виртуальные машины
`cd infra/yandex_cloud`

`terraform destroy`
